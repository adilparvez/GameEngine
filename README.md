# GameEngine

A game engine written in Java to explore the basics of game programming. Uses the programmable graphics pipeline.

![screenshot](/game-engine-screenshot.png)

![](/gif.gif)