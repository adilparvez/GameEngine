package textures;

public class ModelTexture {
	
	private int textureId;
	
	private float shineDamping = 1;
	private float reflectivity = 0;
	
	private boolean transparency = false;
	private boolean fakeLighting = false;
	
	public ModelTexture(int textureId) {
		this.textureId = textureId;
	}
	
	public int getTextureId() {
		return textureId;
	}

	public float getShineDamping() {
		return shineDamping;
	}

	public void setShineDamping(float shineDamping) {
		this.shineDamping = shineDamping;
	}

	public float getReflectivity() {
		return reflectivity;
	}

	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}
	
	public boolean hasTransparency() {
		return transparency;
	}

	public void setTransparency(boolean transparency) {
		this.transparency = transparency;
	}
	
	public boolean hasFakeLighting() {
		return fakeLighting;
	}

	public void setFakeLighting(boolean fakeLighting) {
		this.fakeLighting = fakeLighting;
	}

}
