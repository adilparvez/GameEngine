package entities;

import models.TexturedModel;

import org.lwjgl.util.vector.Vector3f;

public class Entity {

	private TexturedModel model;
	private Vector3f position;
	private Vector3f orientation;
	private float scaleFactor;
	
	public Entity(TexturedModel model, Vector3f position, Vector3f orientation, float scaleFactor) {
		this.model = model;
		this.position = position;
		this.orientation = orientation;
		this.scaleFactor = scaleFactor;
	}
	
	public TexturedModel getTexturedModel() {
		return model;
	}
	
	public void updatePositionBy(float dx, float dy, float dz) {
		position.x += dx;
		position.y += dy;
		position.z += dz;
	}
	
	public void updateOrientationBy(float drx, float dry, float drz) {
		orientation.x += drx;
		orientation.y += dry;
		orientation.z += drz;
	}
	
	public Vector3f getPosition() {
		return position;
	}
	
	public void setPosition(Vector3f position) {
		this.position = position;
	}
	
	public Vector3f getOrientation() {
		return orientation;
	}
	
	public void setOrientation(Vector3f orientation) {
		this.orientation = orientation;
	}
	
	public float getRotX() {
		return orientation.x;
	}
	
	public void setRotX(float rx) {
		orientation.x = rx;
	}
	
	public float getRotY() {
		return orientation.y;
	}
	
	public void setRotY(float ry) {
		orientation.y = ry;
	}
	
	public float getRotZ() {
		return orientation.z;
	}
	
	public void setRotZ(float rz) {
		orientation.z = rz;
	}
	
	public float getScaleFactor() {
		return scaleFactor;
	}
	
	public void setScaleFactor(float scaleFactor) {
		this.scaleFactor = scaleFactor;
	}
	
}
