package renderengine;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import models.RawModel;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class Loader {
	
	private static Loader instance;
	private ArrayList<Integer> vaoIds = new ArrayList<Integer>();
	private ArrayList<Integer> vboIds = new ArrayList<Integer>();
	private ArrayList<Integer> textureIds = new ArrayList<Integer>();
	
	private Loader() {}
	
	public static Loader getInstance() {
		if (instance != null) {
			return instance;
		}
		instance = new Loader();
		return instance;
	}
	
	public RawModel loadIntoVao(float[] positions, float[] textureCoordinates, float[] normals, int[] indices) {
		int vaoId = createVao();
		bindIndexVbo(indices);
		loadDataIntoAttributeList(0, 3, positions);
		loadDataIntoAttributeList(1, 2, textureCoordinates);
		loadDataIntoAttributeList(2, 3, normals);
		unbindVao();
		return new RawModel(vaoId, indices.length);
	}
	
	public void bindIndexVbo(int[] indices) {
		int vboId = GL15.glGenBuffers();
		vboIds.add(vboId);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboId);
		IntBuffer buffer = intArrayToIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
	}
	
	public int loadTexture(String fileName) {
		Texture texture = null;
		try {
			texture = TextureLoader.getTexture("PNG", new FileInputStream("res/" + fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		int textureId = texture.getTextureID();
		textureIds.add(textureId);
		return textureId;
	}
	
	public void clearVaosVbosTextures() {
		for (int vaoId : vaoIds) {
			GL30.glDeleteVertexArrays(vaoId);
		}
		for (int vboId : vboIds) {
			GL15.glDeleteBuffers(vboId);
		}
		for (int textureId : textureIds) {
			GL11.glDeleteTextures(textureId);
		}
	}
	
	private int createVao() {
		int vaoId = GL30.glGenVertexArrays();
		vaoIds.add(vaoId);
		GL30.glBindVertexArray(vaoId);
		return vaoId;
	}
	
	private void loadDataIntoAttributeList(int attributeNumber, int coordinateSize, float[] data) {
		int vboId = GL15.glGenBuffers();
		vboIds.add(vboId);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
		FloatBuffer buffer = floatArrayToFloatBuffer(data);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attributeNumber, coordinateSize, GL11.GL_FLOAT, false, 0, 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}
	
	private void unbindVao() {
		GL30.glBindVertexArray(0);
	}
	
	private FloatBuffer floatArrayToFloatBuffer(float[] data) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	private IntBuffer intArrayToIntBuffer(int[] data) {
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}

}
