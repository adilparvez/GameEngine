package renderengine;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

public class DisplayManager {
	
	private static DisplayManager instance;
	private static int width;
	private static int height;
	private static String title;
	
	private DisplayManager() {}
	
	public static DisplayManager getInstance() {
		if (instance != null) {
			return instance;
		}
		instance = new DisplayManager();
		return instance;
	}
	
	public DisplayManager setDisplaySize(int width, int height) {
		DisplayManager.width = width;
		DisplayManager.height = height;
		return this;
	}

	public DisplayManager setTitle(String title) {
		DisplayManager.title = title;
		return this;
	}
	
	public void createDisplay() {
		ContextAttribs attribs = new ContextAttribs(3, 2)
		.withForwardCompatible(true)
		.withProfileCore(true);
		
		try {
			Display.setDisplayMode(new DisplayMode(width, height));
			Display.setTitle(title);
			Display.create(new PixelFormat(), attribs);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		
		GL11.glViewport(0, 0, width, height);
	}
	
	public void updateDisplay() {
		Display.sync(120);
		Display.update();
	}
	
	public void destroyDisplay() {
		Display.destroy();
	}

	public boolean isCloseRequested() {
		return Display.isCloseRequested();
	}
}
