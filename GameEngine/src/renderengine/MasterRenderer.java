package renderengine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import models.TexturedModel;
import shaders.StaticShader;
import shaders.TerrainShader;
import terrains.Terrain;
import entities.Camera;
import entities.Entity;
import entities.Light;

public class MasterRenderer {
	
	private static final float FOV = 70;
	private static final float NEAR_PLANE = 0.1f;
	private static final float FAR_PLANE = 1000.0f;
	
	private static final Vector3f skyColor = new Vector3f(0.5f, 0.5f, 0.5f);
	
	private Matrix4f projectionMatrix;
	
	private StaticShader staticShader = new StaticShader();
	private EntityRenderer entityRenderer;
	
	private TerrainShader terrainShader = new TerrainShader();
	private TerrainRenderer terrainRenderer;
	
	private Map<TexturedModel, List<Entity>> entities = new HashMap<TexturedModel, List<Entity>>();
	private List<Terrain> terrains = new ArrayList<Terrain>();
	
	public MasterRenderer() {
		enableBackFaceCulling();
		createProjectionMatrix();
		entityRenderer = new EntityRenderer(staticShader, projectionMatrix);
		terrainRenderer = new TerrainRenderer(terrainShader, projectionMatrix);
	}
	
	public static void enableBackFaceCulling() {
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
	}
	
	public static void disableBackFaceCulling() {
		GL11.glDisable(GL11.GL_CULL_FACE);
	}
	
	public void render(Light sun, Camera camera) {
		clear();
		
		staticShader.start();
		staticShader.loadSkyColor(skyColor);
		staticShader.loadLight(sun);
		staticShader.loadViewMatrix(camera);
		entityRenderer.render(entities);
		staticShader.stop();
		entities.clear();
		
		terrainShader.start();
		terrainShader.loadSkyColor(skyColor);
		terrainShader.loadLight(sun);
		terrainShader.loadViewMatrix(camera);
		terrainRenderer.render(terrains);
		terrainShader.stop();
		terrains.clear();		
	}
	
	public void processTerrain(Terrain terrain) {
		terrains.add(terrain);
	}
	
	public void processEntity(Entity entity) {
		TexturedModel model = entity.getTexturedModel();
		List<Entity> batch = entities.get(model);
		if (batch != null) {
			batch.add(entity);
		} else {
			batch = new ArrayList<Entity>();
			batch.add(entity);
			entities.put(model, batch);
		}
	}
	
	public void cleanUp() {
		staticShader.detatchAndDelete();
		terrainShader.detatchAndDelete();
	}
	
	public void clear() {
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClearColor(skyColor.x, skyColor.y, skyColor.z, 1.0f);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
	}
	
	private void createProjectionMatrix() {
		float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
		float yScale = (float) ((1.0f / Math.tan(Math.toRadians(FOV / 2.0f))) * aspectRatio);
		float xScale = yScale / aspectRatio;
		float frustumLength = FAR_PLANE - NEAR_PLANE;
		
		projectionMatrix = new Matrix4f();
		projectionMatrix.m00 = xScale;
		projectionMatrix.m11 = yScale;
		projectionMatrix.m22 = - (FAR_PLANE + NEAR_PLANE) / frustumLength;
		projectionMatrix.m23 = -1.0f;
		projectionMatrix.m32 = - (2 * FAR_PLANE * NEAR_PLANE) / frustumLength;
	}
}
