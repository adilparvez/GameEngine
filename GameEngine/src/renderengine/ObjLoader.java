package renderengine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import models.RawModel;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class ObjLoader {
	
	public static RawModel loadObjModel(String fileName) {
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(new File("res/" + fileName));
		} catch (FileNotFoundException e) {
			System.err.println("[ERROR] Could not find OBJ file.");
			e.printStackTrace();
		}
		BufferedReader reader = new BufferedReader(fileReader);
		String line;
		List<Vector3f> vertices = new ArrayList<Vector3f>();
		List<Vector2f> textures = new ArrayList<Vector2f>();
		List<Vector3f> normals = new ArrayList<Vector3f>();
		List<Integer> indices = new ArrayList<Integer>();
		float[] vertexArray;
		float[] textureArray = null;
		float[] normalArray = null;
		int[] indexArray;
		try {
			while (true) {
				line = reader.readLine();
				String[] explodedLine = line.split(" ");
				if (line.startsWith("v ")) {
					Vector3f vertex = new Vector3f(Float.parseFloat(explodedLine[1]), Float.parseFloat(explodedLine[2]), Float.parseFloat(explodedLine[3]));
					vertices.add(vertex);
				} else if (line.startsWith("vt ")) {
					Vector2f texture = new Vector2f(Float.parseFloat(explodedLine[1]), Float.parseFloat(explodedLine[2]));
					textures.add(texture);
				} else if (line.startsWith("vn ")) {
					Vector3f normal = new Vector3f(Float.parseFloat(explodedLine[1]), Float.parseFloat(explodedLine[2]), Float.parseFloat(explodedLine[3]));
					normals.add(normal);
				} else if (line.startsWith("f ")) {
					textureArray = new float[vertices.size() * 2];
					normalArray = new float[vertices.size() * 3];
					break;
				}
			}
			while (line != null) {
				if (!line.startsWith("f")) {
					line = reader.readLine();
					continue;
				}
				String[] explodedLine = line.split(" ");
				String[] vertex1 = explodedLine[1].split("/");
				String[] vertex2 = explodedLine[2].split("/");
				String[] vertex3 = explodedLine[3].split("/");
				processVertex(vertex1, indices, textures, normals, textureArray, normalArray);
				processVertex(vertex2, indices, textures, normals, textureArray, normalArray);
				processVertex(vertex3, indices, textures, normals, textureArray, normalArray);
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		vertexArray = new float[vertices.size() * 3];
		int i = 0;
		for (Vector3f vertex : vertices) {
			vertexArray[i++] = vertex.x;
			vertexArray[i++] = vertex.y;
			vertexArray[i++] = vertex.z;
		}
		
		indexArray = new int[indices.size()];
		for (int j = 0; j < indices.size(); j++) {
			indexArray[j] = indices.get(j);
		}
		
		return Loader.getInstance().loadIntoVao(vertexArray, textureArray, normalArray, indexArray);
	}

	private static void processVertex(String[] vertex, List<Integer> indices, List<Vector2f> textures, List<Vector3f> normals, float[] textureArray, float[] normalArray) {
		int vertexPointer = Integer.parseInt(vertex[0]) - 1;
		indices.add(vertexPointer);
		Vector2f texture = textures.get(Integer.parseInt(vertex[1]) - 1);
		textureArray[vertexPointer * 2] = texture.x;
		textureArray[vertexPointer * 2 + 1] = 1 - texture.y;
		Vector3f normal = normals.get(Integer.parseInt(vertex[2]) - 1);
		normalArray[vertexPointer * 3] = normal.x;
		normalArray[vertexPointer * 3 + 1] = normal.y;
		normalArray[vertexPointer * 3 + 2] = normal.z;	
	}

}
