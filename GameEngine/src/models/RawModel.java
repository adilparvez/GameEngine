package models;

public class RawModel {

	private int vaoId;
	private int numberOfVertices;
	
	public RawModel(int vaoId, int numberOfVertices) {
		this.vaoId = vaoId;
		this.numberOfVertices = numberOfVertices;
	}
	
	public int getVaoId() {
		return vaoId;
	}
	
	public int getNumberOfVertices() {
		return numberOfVertices;
	}
}
