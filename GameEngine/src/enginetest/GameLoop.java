package enginetest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import models.TexturedModel;

import org.lwjgl.util.vector.Vector3f;

import renderengine.DisplayManager;
import renderengine.Loader;
import renderengine.MasterRenderer;
import renderengine.ObjLoader;
import terrains.Terrain;
import textures.ModelTexture;
import entities.Camera;
import entities.Entity;
import entities.Light;

public class GameLoop {
	
	public static void main(String[] args) {
		DisplayManager displayManager = DisplayManager.getInstance()
		.setDisplaySize(1280, 670)
		.setTitle("Engine Test");
		
		displayManager.createDisplay();
		
		Loader loader = Loader.getInstance();
		Light sun = new Light(new Vector3f(0, 200, 0), new Vector3f(1, 1, 1));
		Camera camera = new Camera();
		
		TexturedModel pine = new TexturedModel(ObjLoader.loadObjModel("pine.obj"), new ModelTexture(loader.loadTexture("pine.png")));
		pine.getModelTexture().setTransparency(false);
		
		TexturedModel fern = new TexturedModel(ObjLoader.loadObjModel("fern.obj"), new ModelTexture(loader.loadTexture("fern.png")));
		fern.getModelTexture().setTransparency(true);
		
		TexturedModel grass = new TexturedModel(ObjLoader.loadObjModel("grass.obj"), new ModelTexture(loader.loadTexture("grass.png")));
		grass.getModelTexture().setTransparency(true);
		grass.getModelTexture().setFakeLighting(true);
		
		List<Entity> entities = new ArrayList<Entity>();
		Random random = new Random();
		for (int i = 0; i < 2000; i++) {
			entities.add(new Entity(pine, new Vector3f(random.nextFloat()*400-200,0,random.nextFloat()*400-200), new Vector3f(0,random.nextFloat()*180,0), 1.0f + 2 * random.nextFloat()));
			entities.add(new Entity(fern, new Vector3f(random.nextFloat()*400-200,0,random.nextFloat()*400-200), new Vector3f(0,random.nextFloat()*180,0), 0.5f + random.nextFloat()));
			entities.add(new Entity(grass, new Vector3f(random.nextFloat()*400-200,0,random.nextFloat()*400-200), new Vector3f(0,random.nextFloat()*180,0), 0.5f + random.nextFloat() ));
		}
		
		List<Terrain> terrains = new ArrayList<Terrain>();
		for (int x = -2; x <= 2; x++) {
			for (int z = -2; z <= 2; z++) {
				terrains.add(new Terrain(x, z, new ModelTexture(loader.loadTexture("terrain.png"))));
			}
		}
		
		
		MasterRenderer renderer = new MasterRenderer();	
		while (!displayManager.isCloseRequested()) {
			camera.move();
			
			for (Entity entity : entities) {
				renderer.processEntity(entity);
			}
			for (Terrain terrain : terrains) {
				renderer.processTerrain(terrain);
			}
			
			renderer.render(sun, camera);
			displayManager.updateDisplay();
		}
		renderer.cleanUp();
		loader.clearVaosVbosTextures();
		displayManager.destroyDisplay();
	}

}
