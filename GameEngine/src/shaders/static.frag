#version 330 core

in vec2 pass_textureCoordinates;
in vec3 surfaceNormal;
in vec3 toLightVector;
in vec3 toCameraVector;
in float visibility;

out vec4 out_Color;

uniform sampler2D textureSampler;
uniform vec3 lightColor;
uniform float shineDamping;
uniform float reflectivity;
uniform vec3 skyColor;

void main(void) {
  vec3 unitNormal = normalize(surfaceNormal);
  vec3 unitToLightVector = normalize(toLightVector);
  float dot1 = dot(unitNormal, unitToLightVector);
  float brightness = max(dot1, 0.2);
  vec3 diffuse = brightness * lightColor;

  vec3 unitToCameraVector = normalize(toCameraVector);
  vec3 lightDirection = - unitToLightVector;
  vec3 refletedLightDirection = reflect(lightDirection, unitNormal);
  float dot2 = dot(refletedLightDirection, unitToCameraVector);
  float specularFactor = max(dot2, 0);
  float dampedFactor = pow(specularFactor, shineDamping);
  vec3 specular = dampedFactor * reflectivity * lightColor;

  vec4 textureColor = texture(textureSampler, pass_textureCoordinates);
  if (textureColor.a <= 0.5) {
    discard;
  }
 
  out_Color = vec4(diffuse, 1.0) * textureColor + vec4(specular, 1.0); 
  out_Color = mix(vec4(skyColor, 1.0), out_Color, visibility);
}